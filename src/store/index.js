import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        engine: null,
        scene: null,
    },
    mutations: {
        setEngine(state, value) {
            state.engine = value;
        },
        setScene(state, value) {
            state.scene = value;
        },
    },
    actions: {
        setCoreEngine({ commit }, { engine, scene }) {
            commit("setEngine", engine);
            commit("setScene", scene);
        },
    },
    getters: {
        getEngine(state) {
            return state.engine;
        },
        getScene(state) {
            return state.scene;
        },
    },
    modules: {},
});

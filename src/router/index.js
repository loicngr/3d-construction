import Vue from "vue";
import VueRouter from "vue-router";
import Render from "../views/Render.vue";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Render",
        component: Render,
    },
    /*{
        path: "/about",
        name: "About",
        component: () => import(/!* webpackChunkName: "about" *!/ "../views/About.vue"),
    },*/
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
